
const userProfileTab = require('./userProfileTab.png')
const rightArrow = require('./rightArrow.png')
const search = require('./search.png')
const home = require('./home.png')
const order = require('./order.png')
const wishlist = require('./wishlist.png')
const account = require('./account.png')
const french = require('./french.png')
const menu = require('./menu.png')
const cart = require('./cart.png')
const user = require('./user.png')
const plus = require('./plus.png')
const minus = require('./minus.png')
const success = require('./success.png')
const marker = require('./marker.png')
const gps = require('./gps.png')
const select = require('./select.png')
const food = require('./food.jpg')
const foodcover = require('./foodcover.jpg')
const error = require('./error.png')

export const images = {
    userProfileTab,
    rightArrow,
    search,
    home,
    order,
    wishlist,
    account,
    french,
    menu,
    cart,
    user,
    plus,
    minus,
    success,
    marker,
    gps,
    select,
    food,
    foodcover,
    error,
};