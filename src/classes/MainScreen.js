import React, { Component } from 'react'
import { View, TouchableOpacity, Text, TextInput, Image, Dimensions, KeyboardAvoidingView, ScrollView, Platform } from 'react-native'
import { images } from '../../Assets'
import Constant from '../../Constant';
export default class MainScreen extends React.Component {


    render() {
        var screenWidth;
        if (Dimensions.get('window').height < 819) {
            screenWidth = 65
        } else {
            screenWidth = 90
        }
        return (
            <KeyboardAvoidingView
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center'
                }}
                behavior={Platform.OS === "ios" ? "padding" : null}
                enabled
                keyboardVerticalOffset={Platform.OS === 'ios' ? screenWidth : 0
                }>
                <View style={{ backgroundColor: 'white', flex: 1, paddingBottom: 30 }}>
                    <ScrollView
                        contentContainerStyle={{
                            flexGrow: 1,
                            flexDirection: 'column',
                            justifyContent: 'space-between'
                        }}
                        ref='ScrollView_Reference'
                        keyboardShouldPersistTaps='handled'>
                        <View style={{ width: '100%', backgroundColor: 'white' }}>

                            <View style={{ marginTop: 80, alignItems: 'center' }}>
                                <View style={{ height: 80, width: 80, backgroundColor: '#cc1dba', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 44 }}>MB</Text>
                                </View>
                                <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 22 }}>{Constant.AppText.appName}</Text>
                            </View>
                            <View style={{ marginLeft: 18, marginRight: 18, marginTop: 60 }}>
                                <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 22, textAlign: 'center' }}>Need groceries or food supplies delivered? Get it Dun!</Text>
                                <View style={{ marginLeft: 32, marginRight: 32, marginTop: 10 }}>
                                    <Text style={{ color: 'black', fontWeight: '200', fontSize: 16, textAlign: 'center' }}>All items have real freshness and are intended for your needs</Text>
                                </View>
                            </View>
                            <View style={{ padding: 20, paddingBottom: 5, marginTop: 80 }}>
                                <View style={{ width: '100%', borderRadius: 25, backgroundColor: '#cc1dba', flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={{ padding: 10, fontSize: 16, color: 'white' }}>+44</Text>
                                    <View style={{ flex: 1 }}>
                                        <TextInput
                                            editable = {false}
                                            maxLength={10}
                                            keyboardType='numeric'
                                            placeholderTextColor='white'
                                            placeholder='Enter your phone number'
                                        />
                                    </View>
                                    <TouchableOpacity
                                    // onPress={() => { this.props.navigation.navigate('App') }}
                                    >
                                        <Image
                                            style={{ width: 28, height: 28, marginRight: 10, tintColor: 'white' }}
                                            source={images.rightArrow} />
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <View style={{ alignItems: 'center' }}>
                                <Text style={{ fontSize: 16, color: 'gray' }}>or</Text>
                            </View>
                            <View style={{ margin: 10, flexDirection: 'row' }}>
                                <TouchableOpacity
                                    onPress={() => { this.props.navigation.navigate('Signup') }}
                                    style={{ flex: 1, backgroundColor: '#cc1dba', alignItems: 'center', padding: 10,margin:5 }}>
                                    <Text style={{ color: 'white', fontWeight: 'bold' }}>SIGNUP</Text>
                                </TouchableOpacity>

                                {/* <View style={{ alignItems: 'center' }}>
                                <Text style={{ fontSize: 16, color: 'gray' }}>or</Text>
                            </View> */}

                                    <TouchableOpacity
                                        onPress={() => { this.props.navigation.replace('Login') }}

                                        style={{flex:1, backgroundColor: 'white', borderWidth: 1, borderColor: 'black', alignItems: 'center', padding: 10 ,margin:5}}>
                                        <Text style={{ color: 'black', fontWeight: 'bold' }}>LOGIN</Text>
                                    </TouchableOpacity>
                            </View>

                        </View>
                        <View style={{ alignItems: 'center', marginTop: 20 }}>
                            <Text style={{ color: 'gray' }}>By continuing. you accept all terms & condition</Text>

                        </View>
                    </ScrollView>
                </View>
            </KeyboardAvoidingView>
        )
    }
}