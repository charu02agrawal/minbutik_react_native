import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from "react-native";
import { images } from '../../Assets';

export default class Track extends React.Component {
    render() {
        return (
            <View >
                <View style={{ marginTop: 80, padding: 20 }}>
                    <Image
                        source={images.marker}
                        style={{ width: 28, height: 28, tintColor: 'black' }} />
                    <View style={{ marginTop: 10 }}>
                        <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 26 }}>Nice to</Text>
                        <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 26 }}>meet you!</Text>
                    </View>
                    <Text style={{ color: 'gray', marginTop: 10 }}>Set your location to start find store's around you</Text>

                </View>
                <View style={{ padding: 20, alignItems: 'center' }}>
                    <View
                        style={{ flexDirection: 'row', width: '100%', marginTop: 50, backgroundColor: '#cc1dba', borderRadius: 5, alignItems: 'center', padding: 10 }}>
                        <View style={{ alignItems: 'center', width: '100%' }}>
                            <TouchableOpacity
                                onPress={() => { this.props.navigation.navigate('App') }}
                            >
                                <View
                                    style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Image
                                        source={images.gps}
                                        style={{ width: 15, height: 15, tintColor: 'white', marginRight: 20 }}
                                    />
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 16, }}>Use current location</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 18, marginTop: 40 }}>Set your location manually</Text>
                </View>
            </View>
        )
    }
}