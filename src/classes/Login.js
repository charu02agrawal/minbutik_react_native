import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from "react-native";
import { TextInput } from 'react-native-gesture-handler';
import Loader from './Loader';
import { _storeData } from '../sharedPrefernce/SharedPreference'
import Constant from '../../Constant';

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email:'',
            password:'',
            isLoading: false,
            error:'',
        };

    }

    isEmptyInputField() {
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var isEmpty = true
        this.setState({ error: '' })

        if (this.state.email.trim().length!=10) {
            isEmpty = true
            this.setState({ error: 'Please enter valid phone number' })
        } else if (this.state.password.trim().length === 0) {
            this.setState({ error: 'Password is required' })
            isEmpty = true
        }
        else {
            isEmpty = false
        }
        console.log("Empty field value ", isEmpty)

        return isEmpty
    }
    postDataOnServer = () => {
        if (this.isEmptyInputField() === false) {

            const { navigate } = this.props.navigation;

            this.setState({ isLoading: true })
            // let data = this.createFormData();

            let url = `${Constant.API.BASE_URL}${Constant.API.LOGIN}`
            console.log("RUk API URL", url)
            console.log("RUk API URL", this.state.email)
            console.log("RUk API URL", this.state.email)

            fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                
                    password: this.state.password,
                    mobile: this.state.email
                })
            }).then(function (response) {
                console.log('response', response)
                return response.json();
            }).then(function (result) {
                console.log(result)
                return result;
            
            }).then(result => {
                console.log("RUK", result.status)
                if (result.status === 'true') {
                    console.log('userId*****',result.data.id)
                    _storeData('LoginStatus', 'Loggedin')
                    _storeData('UserId', result.data.id)

                    this.props.navigation.replace('Track')
                } else {
                    this.setState({ error: result.message })
                }

            })
                .then(isLoading => {
                    this.setState({ isLoading: false });
                })
                .catch(error => console.log("-------- error ------- " + error))

        }
        }
    render() {
        return (
            <View style={{ backgroundColor: 'white', width: '100%', height: '100%' }}>

            <View>
                <View style={{ margin: 15 }}>
                    {/* <View style={{ marginBottom: 10, marginTop: 30 }}>
                        <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 26 }}>Sign UP</Text>
                    </View> */}
                    <View style={{ marginTop: 50, marginBottom: 10 }}>
                            <Text style ={{paddingVertical:5,color:'red'}}>{this.state.error}</Text>
                            <Text>{Constant.AppText.mobile}</Text>
                            <TextInput
                                maxLength = {10}
                                onChangeText={(email) =>
                                    this.setState({ email })}
                                keyboardType='email-address'
                                autoCapitalize='none'
                            style={{ borderBottomColor: 'gray', borderBottomWidth: 2 }}
                        />
                    </View>
                    <View style={{ marginVertical: 10 }}>
                        <Text>{Constant.AppText.password}</Text>
                            <TextInput
                                onChangeText={(password) =>
                                    this.setState({ password})}
                            secureTextEntry={true}
                            style={{ borderBottomColor: 'gray', borderBottomWidth: 2 }}
                        />
                    </View>
                        <TouchableOpacity
                            onPress ={() => this.postDataOnServer()}
                        // onPress={() => { this.props.navigation.navigate('App') }}
                        style={{ width: '100%', marginTop: 20, backgroundColor: '#cc1dba', borderRadius: 5, alignItems: 'center', padding: 10 }}>
                        <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 18 }}>{Constant.AppText.login}</Text>
                    </TouchableOpacity>
                </View>
            </View>
             {
            this.state.isLoading &&
            <Loader loading={true} color="#0D788B" size="large"></Loader>
        }
            </View >
        )
    }
}