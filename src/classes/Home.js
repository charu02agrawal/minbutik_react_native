import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, ImageBackground, PermissionsAndroid } from "react-native";
import { images } from '../../Assets';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Geolocation from '@react-native-community/geolocation';
import Loader from './Loader';
import Constant from '../../Constant';

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            topPick: [],
            isLoading: false,
            imagesUrl: '',
            allCategoryImagesUrl:''
        };
    }

    componentDidMount() {

        this._navListener = this.props.navigation.addListener('didFocus', () => {
            // get your new data here and then set state it will rerender
            console.log("did focus history");
            this.setState  ({
                data: [],
                    topPick: [],
                        isLoading: false,
                            imagesUrl: '',
            }
        )
            this.getDataFromServer()

            console.log('componentDidMount')
            this.setState({ isLoading: true })
            var that = this;
            //Checking for the permission just after component loaded
            if (Platform.OS === 'ios') {
                this.callLocation(that);
            } else {
                async function requestLocationPermission() {
                    try {
                        const granted = await PermissionsAndroid.request(
                            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
                            'title': 'Location Access Required',
                            'message': 'This App needs to Access your location'
                        }
                        )
                        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                            //To Check, If Permission is granted
                            that.callLocation(that);
                        } else {
                            alert("Permission Denied");
                        }
                    } catch (err) {
                        // alert("err", err);
                        console.warn(err)
                    }
                }
                requestLocationPermission();
            }
        });
    }
    callLocation(that) {
        console.log('location called')
        //alert("callLocation Called");
        Geolocation.getCurrentPosition(
            //Will give you the current location
            (position) => {
                const currentLongitude = JSON.stringify(position.coords.longitude);
                //getting the Longitude from the location json
                const currentLatitude = JSON.stringify(position.coords.latitude);
                //getting the Latitude from the location json
                that.setState({ currentLongitude: currentLongitude });
                //Setting state Longitude to re re-render the Longitude Text
                that.setState({ currentLatitude: currentLatitude });
                //Setting state Latitude to re re-render the Longitude Text
                this.getDataNearByStore()
            },
            (error) => {
                this.setState({ isLoading: false });

                // alert(error.message)
            },            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
        that.watchID = Geolocation.watchPosition((position) => {
            //Will give you the location on location change
            console.log(position);
            const currentLongitude = JSON.stringify(position.coords.longitude);
            //getting the Longitude from the location json
            const currentLatitude = JSON.stringify(position.coords.latitude);
            //getting the Latitude from the location json
            that.setState({ currentLongitude: currentLongitude });
            //Setting state Longitude to re re-render the Longitude Text
            that.setState({ currentLatitude: currentLatitude });
            //Setting state Latitude to re re-render the Longitude Text
            this.getDataNearByStore()


        });
        // if (this.state.currentLatitude != 'unknown' && this.state.currentLongitude != 'unknown') {
        //     this.getDataNearByStore()
        // }
        // this.getDataNearByStore()

    }
    componentWillUnmount = () => {
        this._navListener.remove();

        Geolocation.clearWatch(this.watchID);
    }




    getDataNearByStore() {
        let url = `${Constant.API.BASE_URL}${Constant.API.STORE_NEAR_BY}`
        console.log(this.state.currentLatitude)
        console.log(this.state.currentLongitude)

        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                latitudes: this.state.currentLatitude,
                longitude: this.state.currentLongitude,
                // latitudes: '22.7196',
                // longitude: '75.8577',
                mile: 300,
            })
        }).then(function (response) {
            console.log('response', response)
            return response.json();
        })
            .then(function (result) {
                return result;

            }).then(result => {
                console.log('near store result', result)
                if (result.status === 'true') {
                    this.setState({
                        topPick: result.data,
                        imagesUrl: result.imagesUrl
                    })
                }

            })
            .then(isLoading => {
                this.setState({ isLoading: false });
            })
            .catch(error => {
                this.setState({ isLoading: false });
                console.log("-------- error ------- " + error)
            })

    }

    getDataFromServer() {
        let url = `${Constant.API.BASE_URL}${Constant.API.CATEGORY_ALL_DATA}`
        fetch(url, { method: 'GET' }).then(function (response) {
            return response.json();
        })
            .then(function (result) {
                return result;

            }).then(result => {
                console.log('result all category', result)
                if (result.status === 'true') {
                    console.log('result status', result.status)

                    this.setState({ data: result.data, allCategoryImagesUrl: result.imagesUrl })
                }
            })
            .then(isLoading => {
                this.setState({ isLoading: false });
            })
            .catch(error => console.log("-------- error ------- " + error))

    }
    render() {
        console.log('length', this.state.imagesUrl)
        return (
            <View style={{ backgroundColor: 'white', width: '100%', height: '100%' }}>
                <ScrollView>
                    <View style={{ padding: 15 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: 10 }}>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <Image
                                    style={{ width: 22, height: 22, margin: 5, tintColor: 'black' }}
                                    source={images.menu} />
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity
                                    onPress={() => { this.props.navigation.navigate('Cart') }}>
                                    <View>
                                        <Image
                                            style={{ width: 25, height: 25, margin: 5, tintColor: 'black' }}
                                            source={images.cart} />
                                        {/* <View style={{ position: 'absolute', right: 0, width: 15, height: 15, borderRadius: 360, backgroundColor: '#cc1dba', alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: 'white' }}>1</Text>
                                        </View> */}
                                    </View>
                                </TouchableOpacity>
                                <Image
                                    style={{ width: 25, height: 25, margin: 5, marginLeft: 20, tintColor: 'black' }}
                                    source={images.user} />
                            </View>
                        </View>
                        <View style={{}}>
                            <View style={{
                                flexDirection: 'row',
                                marginVertical: 10, elevation: 5,
                                backgroundColor: '#DCDCDC',
                                shadowOffset: { width: 0, height: 1 },
                                shadowColor: '#333',
                                shadowOpacity: 0.3,
                                shadowRadius: 2,
                                justifyContent: 'space-around',
                                alignItems: 'center'
                            }}>
                                <Image style={{ width: 20, height: 20, marginHorizontal: 15, resizeMode: 'cover', tintColor: 'gray' }}
                                    source={images.search}
                                />
                                <View style={{ flex: 1 }}>
                                    <TextInput
                                        placeholder="Search for iems and Store"
                                    />
                                </View>
                            </View>
                            {this.state.data.length > 0 &&
                                <Text style={{ fontWeight: 'bold', fontSize: 18, color: 'black', }}>{Constant.AppText.topCategory}</Text>
                            }
                            <View style={{ marginTop: 10, marginLeft: -5 }}>
                                <FlatList
                                    data={this.state.data}
                                    // numColumns={3}
                                    horizontal={true}
                                    renderItem={({ item }) =>
                                        <TouchableOpacity
                                            onPress={() => { this.props.navigation.navigate('CategoryScreen', { cate_id: item.id }) }}
                                            style={{ flex: 1, height: 150, margin: 8, flexDirection: 'row' }}>
                                            <View style={{

                                                width: '100%',
                                                height: 150, elevation: 5,
                                                shadowOffset: { width: 0, height: 1 },
                                                shadowColor: '#333',
                                                shadowOpacity: 0.3,
                                                shadowRadius: 2,
                                                justifyContent: 'space-around',
                                                alignItems: 'center'
                                            }}>
                                                <ImageBackground
                                                    source={{ uri: `${this.state.allCategoryImagesUrl}${item.catimg}` }}
                                                    style={{ width: 120, height: 150, paddingBottom: 10, backgroundColor: 'rgba(0,0,0,0.1)' }}>
                                                    <View style={{ flex: 1, justifyContent: 'flex-end', marginHorizontal: 5, }}>
                                                        <Text style={{
                                                            color: 'white', fontSize: 14, fontWeight: 'bold',
                                                        }}>{item.catname}</Text>
                                                        <Image
                                                            style={{ width: 20, height: 20, tintColor: 'white', marginTop: 5 }}
                                                            source={images.rightArrow} />
                                                    </View>
                                                </ImageBackground>

                                            </View>

                                        </TouchableOpacity>
                                    } />
                            </View>
                            {/* <View style={{
                            marginVertical:10,
                            height: 90, elevation: 5,
                            backgroundColor: '#DCDCDC',
                            shadowOffset: { width: 0, height: 1 },
                            shadowColor: '#333',
                            shadowOpacity: 0.3,
                            shadowRadius: 2,
                            justifyContent: 'space-around',
                            alignItems: 'center'
                        }}>
                            <Image style={{ width: "100%", height: 80, resizeMode:'cover' }}
                                source={images.foodcover}
                            />
                        </View> */}
                            
                            {this.state.data.length > 0 &&
                                <Text style={{ fontWeight: 'bold', fontSize: 18, color: 'black', }}>Top picks for you</Text>
                            }
                            <View style={{ marginTop: 10, marginLeft: -5 }}>
                                <FlatList
                                    data={this.state.data}
                                    // numColumns={3}
                                    horizontal={true}
                                    renderItem={({ item }) =>
                                        <TouchableOpacity
                                            onPress={() => { this.props.navigation.navigate('CategoryScreen', { cate_id: item.id }) }}
                                            style={{ flex: 1, height: 150, margin: 8, flexDirection: 'row' }}>
                                            <View style={{

                                                width: '100%',
                                                height: 150, elevation: 5,
                                                shadowOffset: { width: 0, height: 1 },
                                                shadowColor: '#333',
                                                shadowOpacity: 0.3,
                                                shadowRadius: 2,
                                                justifyContent: 'space-around',
                                                alignItems: 'center'
                                            }}>
                                                <ImageBackground
                                                    source={{ uri: `${this.state.allCategoryImagesUrl}${item.catimg}` }}
                                                    style={{ width: 120, height: 150, paddingBottom: 10, backgroundColor: 'rgba(0,0,0,0.1)' }}>
                                                    <View style={{ flex: 1, justifyContent: 'flex-end', marginHorizontal: 5, }}>
                                                        <Text style={{
                                                            color: 'white', fontSize: 14, fontWeight: 'bold',
                                                        }}>{item.catname}</Text>
                                                        <Image
                                                            style={{ width: 20, height: 20, tintColor: 'white', marginTop: 5 }}
                                                            source={images.rightArrow} />
                                                    </View>
                                                </ImageBackground>

                                            </View>

                                        </TouchableOpacity>
                                    } />
                            </View>

                            {this.state.topPick.length > 0 &&
                                <Text style={{ fontWeight: 'bold', fontSize: 18, color: 'black', marginTop: 5 }}>{Constant.AppText.storeNearYou}</Text>
                            }
                            <View style={{ marginTop: 10, paddingBottom: 20, marginLeft: -5 }}>
                                <FlatList
                                    data={this.state.topPick}
                                    numColumns={3}
                                    renderItem={({ item }) =>
                                        <TouchableOpacity
                                            onPress={() => { this.props.navigation.navigate('ProductScreen', { store_id: item.id }) }}
                                            style={{ flex: 1, margin: 8, }}>
                                            <View style={{
                                                height: 90, elevation: 5,
                                                backgroundColor: '#DCDCDC',
                                                shadowOffset: { width: 0, height: 1 },
                                                shadowColor: '#333',
                                                shadowOpacity: 0.3,
                                                shadowRadius: 2,
                                                justifyContent: 'space-around',
                                                alignItems: 'center'
                                            }}>
                                                <Image style={{ width: '100%', height: 90, resizeMode: 'cover' }}
                                                    source={{ uri: `${this.state.imagesUrl}${item.logo}` }}
                                                />
                                            </View>
                                            <View style={{ marginTop: 10 }}>
                                                <Text style={{ color: 'gray' }}>Vegetables</Text>
                                                <Text style={{ color: 'black', fontWeight: 'bold' }}>{item.store_name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    } />
                            </View>

                        </View>
                    </View>
                </ScrollView>
                {
                    this.state.isLoading &&
                    <Loader loading={true} color="#0D788B" size="large"></Loader>
                }
            </View>
        )
    }
}