import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, Platform, PermissionsAndroid } from "react-native";
import { images } from '../../Assets';
import Geolocation from '@react-native-community/geolocation';
import Loader from './Loader'
import Constant from '../../Constant';
import { _retrieveData } from '../sharedPrefernce/SharedPreference'
import { TextInput } from 'react-native-gesture-handler';
import RButton from './RButton'

export default class CartScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            imagesUrl: '',
            isLoading: false,
            noDataFound: false,
            user_id: '',
            radioItems:
                [
                    {
                        label: 'Pick up',
                        size: 22,
                        color: '#cc1dba',
                        selected: false
                    },

                    {
                        label: 'Delivery',
                        color: '#cc1dba',
                        size: 22,
                        selected: true,
                    }
                ],
            selectedItem: ''
        };
    }
    componentDidMount() {

        this._navListener = this.props.navigation.addListener('didFocus', () => {
            // get your new data here and then set state it will rerender
            console.log("did focus history");

            this.setState({
                data: [],
                imagesUrl: '',
                isLoading: false,
                currentLongitude: 'unknown',//Initial Longitude
                currentLatitude: 'unknown',//Initial Latitude
                noDataFound: false
            })
            console.log('componentDidMount')
            this.setState({ isLoading: true })
            _retrieveData('UserId').then(status => {
                console.log('user Id ***** ', status)
                this.setState({ user_id: status })
                this.getDataFromServer()


            }).catch(err => {
                this.setState({ isLoading: false })

                console.log('err')
            })
        });
    }

    changeActiveRadioButton(index) {
        this.state.radioItems.map((item) => {
            item.selected = false;
        });

        this.state.radioItems[index].selected = true;

        this.setState({ radioItems: this.state.radioItems }, () => {
            this.setState({ selectedItem: this.state.radioItems[index].label });
        });
    }

    componentWillUnmount = () => {
        this._navListener.remove();

    }

    getDataFromServer() {
        let url = `${Constant.API.BASE_URL}${Constant.API.CART_DETAIL}`

        console.log('url', url)
        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                // latitudes: this.state.currentLatitude,
                // longitude: this.state.currentLongitude,
                user_id: this.state.user_id,
            })
        }).then(function (response) {
            console.log('response', response)
            return response.json();
        })
            .then(function (result) {
                return result;

            }).then(result => {
                console.log('result', result)
                if (result.status === 'true') {
                    this.setState({
                        data: result.data,
                        imagesUrl: result.imagesUrl
                    })
                } else {
                    this.setState({
                        noDataFound: true,
                    })
                }

            })
            .then(isLoading => {
                this.setState({ isLoading: false });
            })
            .catch(error => console.log("-------- error ------- " + error))

    }
    render() {
        return (
            <View style={{ backgroundColor: 'white', width: '100%', height: '100%' }}>

                <View style={{ padding: 8 }}>
                    <View style={{ margin: 5 }}>
                        {/* {this.state.data.length > 0 && <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>{Constant.AppText.yourOrder}</Text>
                        } */}
                        <View style={{ marginTop: 10, paddingBottom: 20 }}>
                            <FlatList
                                data={this.state.data}
                                numColumns={1}
                                renderItem={({ item }) =>
                                    <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', padding: 15 }}>
                                        <View>
                                            <Image
                                                style={{ width: 55, height: 55 }}
                                                source={{ uri: `${this.state.imagesUrl}${item.pimg}` }} />
                                        </View>
                                        <View style={{ flex: 1, marginLeft: 10 }}>
                                            <Text style={{ textAlignVertical: 'center', fontWeight: 'bold', fontSize: 15, color: 'black' }}>{item.pname.trim()}</Text>
                                            <Text style={{ textAlignVertical: 'center', fontSize: 14, color: 'black' }}>Price : {item.pprice.trim()}</Text>
                                        </View>

                                    </View>
                                }
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    </View>
                    {this.state.data.length > 0 &&
 <View>                       
                    <View style={{ width: '100%', backgroundColor: '#DCDCDC', borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#A9A9A9', paddingTop: 20, paddingBottom: 20, marginTop: 20 }}>
                        <View style={{ flexDirection: 'row', width: '100%', padding: 10, justifyContent: 'center', alignItems: 'center', marginRight: 20 }}>
                            <View style={{ flex: 1 }}>
                                <Text style={{
                                    fontWeight: 'bold', fontSize: 16, color: 'black', padding: 10, marginRight: 20,
                                }}>Coupon Code : </Text>
                            </View>
                            <View style={{ flex: 1 }}>
                                <View style={{ borderColor: 'gray', borderWidth: 1, marginLeft: 13 }}>
                                    <TextInput
                                        placeholder="Your Code"
                                        keyboardType="numeric" />
                                </View>
                            </View>
                        </View>
                   
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            {
                                this.state.radioItems.map((item, key) =>
                                    (
                                        <View style={{ flex: 1 }}>
                                            <View style={{
                                                marginHorizontal: 10,
                                                // padding: 5,
                                                // marginLeft: 20,
                                                elevation: 5,
                                                backgroundColor: 'white',
                                                shadowOffset: { width: 0, height: 1 },
                                                shadowColor: '#333',
                                                shadowOpacity: 0.3,
                                                shadowRadius: 2,
                                                justifyContent: 'space-around',
                                            }}>
                                                <RButton key={key} button={item} onClick={this.changeActiveRadioButton.bind(this, key)} />
                                            </View>
                                        </View>
                                    ))
                            }
                            </View>
                        </View>
                        <TouchableOpacity
                            onPress={() => { this.props.navigation.navigate('Success') }}
                            style={{ marginTop: 35, backgroundColor: '#cc1dba', borderWidth: 1, borderColor: 'black', borderRadius: 5, alignItems: 'center', padding: 10 }}>
                            <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 16 }}>CHECKOUT</Text>
                        </TouchableOpacity>
                    </View>
}

                </View>
                {
                    this.state.isLoading &&
                    <Loader loading={true} color="#0D788B" size="large"></Loader>
                }
                {
                    this.state.noDataFound &&
                    <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            style={{ height: 100, width: 100 }}
                            source={images.error} />
                        <Text style={{ padding: 10, fontSize: 22, fontWeight: 'bold' }}>No record found</Text>
                    </View>
                }
            </View >
        )
    }
}