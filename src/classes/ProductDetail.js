import React, { Component } from 'react';
import { View ,Image,Text} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Constant from '../../Constant';
import Loader from './Loader';
import { _retrieveData } from '../sharedPrefernce/SharedPreference'
import { images } from '../../Assets';

export default class ProductDetail extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            imagesUrl: '',
            isLoading: false,
            qty: 0,
            noDataFound: false,
            item: this.props.navigation.state.params.item,
            imagesUrl: this.props.navigation.state.params.imagesUrl,
            user_id:''
        };
    }
    componentDidMount() {
        this.setState({ isLoading: true })
        _retrieveData('UserId').then(status => {
            console.log('user Id ***** ', status)
            this.setState({ user_id: status })
            this.setState({ isLoading: false })
        }).catch(err => {
            this.setState({ isLoading: false })

            console.log('err')
        })
    }
    addToCart() {
        console.log('item detail',this.state.item)
        let url = `${Constant.API.BASE_URL}${Constant.API.ADD_TO_CART}`
        console.log('url', url)
        this.setState({ isLoading: true });

        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                product_id: this.state.item.id,
                user_id: this.state.user_id,
                qty: this.state.qty,
            })
        }).then(function (response) {
            console.log('response', response)
            return response.json();
        })
            .then(function (result) {
                return result;

            }).then(result => {
                console.log('result', result)
                if (result.status === 'true') {
                  
                } else {
                    this.setState({
                        noDataFound: true,
                    })
                }

            })
            .then(isLoading => {
                this.setState({ isLoading: false });
                this.props.navigation.navigate('Cart')

            })
            .catch(error => console.log("-------- error ------- " + error))

    }

    orderNow() {
        let url = `${Constant.API.BASE_URL}${Constant.API.ORDER_PRODUCT}`
        console.log('url', url)
        console.log('qty', this.state.qty)

        this.setState({ isLoading: true });

        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                product_id: this.state.item.id,
                user_id: this.state.user_id,
                product_name:this.state.item.pname,
                qty:this.state.qty,
                price: this.state.item.pprice
            })
        }).then(function (response) {
            console.log('response', response)
            return response.json();
        })
            .then(function (result) {
                return result;

            }).then(result => {
                console.log('result', result)
                if (result.status === 'true') {
                   
                } else {
                    this.setState({
                        noDataFound: true,
                    })
                }

            })
            .then(isLoading => {
                this.setState({ isLoading: false });
                this.props.navigation.replace('Success')

            })
            .catch(error => console.log("-------- error ------- " + error))

    }

    render() {
        console.log('item ',this.state.item)
        return (
            <View style={{ backgroundColor: 'white', width: '100%', height: '100%' }}>

                <View style={{ padding: 10 }}>
                   
                        <Text style={{ fontWeight: 'bold', fontSize: 18, color: 'black',paddingVertical:20 }}>Item : </Text>
                    
                <View
                    style={{ height: 100, width: '100%', marginBottom: 10, flexDirection: 'row', padding: 2 }}>
                    <View style={{
                        height: 90, width: 120, elevation: 5,
                        backgroundColor: '#DCDCDC',
                        shadowOffset: { width: 0, height: 1 },
                        shadowColor: '#333',
                        shadowOpacity: 0.3,
                        shadowRadius: 2,
                        justifyContent: 'space-around',
                        alignItems: 'center'
                    }}>
                        {/* {console.log('image url', `http://meebasket.com/eko2/${item.logo}`)} */}
                        <Image style={{ width: '100%', height: 80, resizeMode: 'cover' }}
                            source={{ uri: `${this.state.imagesUrl}${this.state.item.pimg}` }}
                        />
                    </View>
                    <View style={{ flex: 1, marginLeft: 20, marginTop: 4 }}>
                        <Text style={{ color: 'gray' }}>Vegetables</Text>
                        <Text style={{ color: 'black', fontWeight: 'bold' }}>{this.state.item.pname.trim()}</Text>
                        <View style={{ flexDirection: 'row', paddingTop: 5 }}>
                            <Text style={{ color: '#cc1dba', fontWeight: 'bold' }}>Price : {this.state.item.pprice}</Text>
                        </View>
                    </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            {/* <View style={{ height: 25, width: 25, borderRadius: 360, borderWidth: 1, borderColor: 'gray', margin: 2 }}></View> */}
                            <TouchableOpacity
                                onPress={() => {
                                    if (this.state.qty > 0) {
                                        console.log('large')
                                        this.setState({ qty: this.state.qty - 1 })
                                    }
                                }}
                                style={{ alignItems: 'center', justifyContent: 'center', margin: 2 }}>
                                <Image
                                    style={{ width: 20, height: 20 }}
                                    source={images.minus} />
                            </TouchableOpacity >
                            <Text style={{ textAlignVertical: 'center', fontWeight: 'bold', fontSize: 16, color: 'black', margin: 2 }}>{this.state.qty}</Text>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ qty: this.state.qty + 1 })

                                }}
                                style={{ alignItems: 'center', justifyContent: 'center', margin: 2 }}>
                                <Image
                                    style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                    source={images.plus} />
                            </TouchableOpacity>
                            {/* <View style={{ height: 25, width: 25, borderRadius: 360, backgroundColor: '#cc1dba', margin: 2 }}></View> */}

                        </View>

                </View>
                <View>
                    <TouchableOpacity
                        onPress={() => this.orderNow()}
                        style={{ width: '100%',  marginTop: 50, backgroundColor: '#cc1dba', borderRadius: 5, alignItems: 'center', padding: 10 }}>
                        <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 16 }}>Order Now</Text>
                    </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.addToCart()}
                        style={{ width: '100%', marginTop:10, backgroundColor: 'white', borderWidth: 1, borderColor: '#cc1dba', borderRadius: 5, alignItems: 'center', padding: 10 }}>
                        <Text style={{ fontWeight: 'bold', color: '#cc1dba', fontSize: 16 }}>Add to cart</Text>
                    </TouchableOpacity>
                </View>
           
                </View>
                {
                    this.state.isLoading &&
                    <Loader loading={true} color="#0D788B" size="large"></Loader>
                }
                {
                    this.state.noDataFound &&
                    <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            style={{ height: 100, width: 100 }}
                            source={images.error} />
                        <Text style={{ padding: 10, fontSize: 22, fontWeight: 'bold' }}>No record found</Text>
                    </View>
                }
            </View >
        )
    }
}