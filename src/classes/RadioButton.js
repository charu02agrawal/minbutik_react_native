import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity,Image } from 'react-native';
import {images} from '../../Assets'

export default class RadioButton extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onClick} activeOpacity={0.8} style={styles.radioButton}>
                <View style={[styles.radioButtonHolder, { height: this.props.button.size, width: this.props.button.size, borderColor: 'gray' }]}>
                    {
                        (this.props.button.selected)
                            ?
                            (
                                <Image
                                    style={{ height: this.props.button.size, width: this.props.button.size}}
                                    source={images.select}/>
                                // <View style={[styles.radioIcon, { height: this.props.button.size, width: this.props.button.size, backgroundColor: this.props.button.color }]}></View>
                            )
                            :
                            null
                    }
                </View>
                <Text style={[styles.label, { color: 'gray' }]}>{this.props.button.label}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    radioButton: {
        flexDirection: 'row',
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    radioButtonHolder: {
        borderRadius: 50,
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    radioIcon: {
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        marginLeft: 10,
        fontSize: 20,
    },

});