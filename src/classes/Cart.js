import React, { Component } from 'react';
import { View, Text, Image, FlatList } from "react-native";
import { images } from '../../Assets'
import { TouchableOpacity, ScrollView, TextInput } from 'react-native-gesture-handler';
import RButton from './RButton'
export default class Cart extends React.Component {
    constructor() {
        super();

        this.state = {
            data: [1, 2, 3],
            qty: 2,
            radioItems:
                [
                    {
                        label: 'Pick up',
                        size: 22,
                        color: '#cc1dba',
                        selected: false
                    },

                    {
                        label: 'Delivery',
                        color: '#cc1dba',
                        size: 22,
                        selected: true,
                    }
                ], selectedItem: ''
        }
    }

    componentDidMount() {
        this.state.radioItems.map((item) => {
            if (item.selected == true) {
                this.setState({ selectedItem: item.label });
            }
        });
    }

    changeActiveRadioButton(index) {
        this.state.radioItems.map((item) => {
            item.selected = false;
        });

        this.state.radioItems[index].selected = true;

        this.setState({ radioItems: this.state.radioItems }, () => {
            this.setState({ selectedItem: this.state.radioItems[index].label });
        });
    }
    render() {
        return (
            <ScrollView>
                <View>
                    <FlatList
                        data={this.state.data}
                        numColumns={1}
                        renderItem={({ item }) =>
                            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', padding: 15 }}>
                                <View>
                                    <Image
                                        style={{ width: 55, height: 55 }}
                                        source={images.french} />
                                </View>
                                <View style={{ flex: 1, marginLeft: 10 }}>
                                    <Text style={{ textAlignVertical: 'center', fontWeight: 'bold', fontSize: 15, color: 'black' }}>Milk</Text>
                                    <Text style={{ textAlignVertical: 'center', fontSize: 14, color: 'black' }}>QTY : 1</Text>

                                </View>
                                <View>
                                    <Text style={{ textAlign: 'right', fontWeight: 'bold', fontSize: 16, color: 'black' }}>Seek 10</Text>
                                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                        {/* <View style={{ height: 25, width: 25, borderRadius: 360, borderWidth: 1, borderColor: 'gray', margin: 2 }}></View> */}
                                        <TouchableOpacity
                                            onPress={() => {
                                                if (this.state.qty > 0) {
                                                    console.log('large')
                                                    this.setState({ qty: this.state.qty - 1 })
                                                }
                                            }}
                                            style={{ alignItems: 'center', justifyContent: 'center', margin: 2 }}>
                                            <Image
                                                style={{ width: 20, height: 20 }}
                                                source={images.minus} />
                                        </TouchableOpacity >
                                        <Text style={{ textAlignVertical: 'center', fontWeight: 'bold', fontSize: 16, color: 'black', margin: 2 }}>{this.state.qty}</Text>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setState({ qty: this.state.qty + 1 })

                                            }}
                                            style={{ alignItems: 'center', justifyContent: 'center', margin: 2 }}>
                                            <Image
                                                style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                                source={images.plus} />
                                        </TouchableOpacity>
                                        {/* <View style={{ height: 25, width: 25, borderRadius: 360, backgroundColor: '#cc1dba', margin: 2 }}></View> */}

                                    </View>
                                </View>
                            </View>
                        }
                    />
                    <View style={{ width: '100%', backgroundColor: '#DCDCDC', borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#A9A9A9', paddingTop: 20, paddingBottom: 20, marginTop: 20 }}>
                        <View style={{ flexDirection: 'row', width: '100%', padding: 10, justifyContent: 'center', alignItems: 'center', marginRight: 20 }}>
                            <View style={{ flex: 1 }}>
                                <Text style={{
                                    fontWeight: 'bold', fontSize: 16, color: 'black', padding: 10, marginRight: 20,
                                }}>Coupon Code : </Text>
                            </View>
                            <View style={{ flex: 1 }}>
                                <View style={{ borderColor: 'gray', borderWidth: 1, marginLeft: 13 }}>
                                    <TextInput
                                        placeholder="Your Code"
                                        keyboardType="numeric" />
                                </View>
                            </View>
                        </View>
                        {/* <View>
                            <View style={{
                                marginTop: 30,
                                marginRight: 20,
                                padding: 10,
                                elevation: 5,
                                backgroundColor: 'white',
                                shadowOffset: { width: 0, height: 1 },
                                shadowColor: '#333',
                                shadowOpacity: 0.3,
                                shadowRadius: 2,
                                justifyContent: 'space-around',
                            }}>
                                
                                <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>Pick up</Text>
                            </View>

                        </View>
                        <View style={{ flex: 1 }}>
                            
                            <View style={{
                                marginTop: 20,
                                padding: 10,
                                marginLeft: 20,
                                elevation: 5,
                                backgroundColor: 'white',
                                shadowOffset: { width: 0, height: 1 },
                                shadowColor: '#333',
                                shadowOpacity: 0.3,
                                shadowRadius: 2,
                                justifyContent: 'space-around',
                            }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>Delivery</Text>
                            </View>

                            </View>
                             */}
                        <View style={{ flexDirection: 'row',marginTop:5 }}>
                            {
                                this.state.radioItems.map((item, key) =>
                                    (
                                        <View style={{ flex: 1 }}>
                                            <View style={{
                                                marginHorizontal: 10,
                                                // padding: 5,
                                                // marginLeft: 20,
                                                elevation: 5,
                                                backgroundColor: 'white',
                                                shadowOffset: { width: 0, height: 1 },
                                                shadowColor: '#333',
                                                shadowOpacity: 0.3,
                                                shadowRadius: 2,
                                                justifyContent: 'space-around',
                                            }}>
                                        <RButton key={key} button={item} onClick={this.changeActiveRadioButton.bind(this, key)} />
                                  </View>
                                            </View>
                                    ))
                            }
                        </View>


                    </View>

                    <View style={{ width: '100%', padding: 20 }}>

                        <View style={{ width: '100%', flexDirection: 'row' }}>
                            <Text style={{ flex: 1 }}>Delivery fees</Text>
                            <Text style={{ flex: 1, textAlign: 'right' }}>SEK 1</Text>
                        </View>
                        <View style={{ width: '100%', flexDirection: 'row', marginTop: 5 }}>
                            <Text style={{ flex: 1, fontWeight: 'bold', color: 'black', fontSize: 16 }}>Total Price</Text>
                            <Text style={{ flex: 1, textAlign: 'right', fontWeight: 'bold', color: 'black', fontSize: 16 }}>SEK 30</Text>
                        </View>
                        <View style={{ width: '100%', flexDirection: 'row', marginTop: 5 }}>
                            <Text style={{ flex: 1 }}>3 items</Text>
                            <Text style={{ flex: 1, textAlign: 'right' }}>Inclusive Tax.</Text>
                        </View>
                    </View>
                    <TouchableOpacity
                        onPress={() => { this.props.navigation.navigate('Success') }}
                        style={{ margin: 15, backgroundColor: '#cc1dba', borderWidth: 1, borderColor: 'black', borderRadius: 5, alignItems: 'center', padding: 10 }}>
                        <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 16 }}>CHECKOUT</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        )
    }
}
