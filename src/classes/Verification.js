import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from "react-native";
import { TextInput } from 'react-native-gesture-handler';

export default class Verification extends React.Component {
    render() {
        return (
            <View>
                <View style={{ margin: 15 }}>
                    <View style={{ marginBottom: 10,marginTop:30 }}>
                        <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 26 }}>Verifying{"\n"}
 your number</Text>
                    </View>
                    <Text>We have send verification code to</Text>
                    <Text>+1234567890</Text>
                    <View style={{ marginTop: 50 }}>
                        <Text>Enter code</Text>
                        <TextInput
                            keyboardType='numeric'
                            style={{ borderBottomColor: 'gray', borderBottomWidth: 2 }}
                        />
                    </View>
                    <TouchableOpacity
                        onPress={() => { this.props.navigation.navigate('Track') }}
                        style={{ width: '100%', marginTop: 20, backgroundColor: '#cc1dba', borderRadius: 5, alignItems: 'center', padding: 10 }}>
                        <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 16 }}>Verify & Continue</Text>
                    </TouchableOpacity>
                    <View style={{flexDirection:'row',marginTop:10}}>
                        <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 16 ,flex:1}}>Resend code</Text>
                        <View>
                            <Text>0:20 Sec</Text>
                            </View>
                    </View>
                </View>
            </View>
        )
    }
}