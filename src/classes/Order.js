import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, Platform, PermissionsAndroid } from "react-native";
import { images } from '../../Assets';
import Geolocation from '@react-native-community/geolocation';
import Loader from './Loader'
import Constant from '../../Constant';
import { _retrieveData } from '../sharedPrefernce/SharedPreference'

export default class Order extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            imagesUrl: '',
            isLoading: false,
            noDataFound: false,
            user_id: '',
        };
    }
    componentDidMount() {

        this._navListener = this.props.navigation.addListener('didFocus', () => {
            // get your new data here and then set state it will rerender
            console.log("did focus history");

            this.setState({
                data: [],
                imagesUrl: '',
                isLoading: false,
                currentLongitude: 'unknown',//Initial Longitude
                currentLatitude: 'unknown',//Initial Latitude
                noDataFound: false
            })
            console.log('componentDidMount')
            this.setState({ isLoading: true })
            _retrieveData('UserId').then(status => {
                console.log('user Id ***** ', status)
                this.setState({ user_id: status })
                this.getDataFromServer()


            }).catch(err => {
                this.setState({ isLoading: false })

                console.log('err')
            })

        });
    }

    componentWillUnmount = () => {
        this._navListener.remove();

    }

    getDataFromServer() {
        let url = `${Constant.API.BASE_URL}${Constant.API.GET_ORDER}`


        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                // latitudes: this.state.currentLatitude,
                // longitude: this.state.currentLongitude,
                user_id: this.state.user_id,
            })
        }).then(function (response) {
            console.log('response', response)
            return response.json();
        })
            .then(function (result) {
                return result;

            }).then(result => {
                console.log('result', result)
                if (result.status === 'true') {
                    this.setState({
                        data: result.data,
                        imagesUrl: result.imagesUrl
                    })
                } else {
                    this.setState({
                        noDataFound: true,
                    })
                }

            })
            .then(isLoading => {
                this.setState({ isLoading: false });
            })
            .catch(error => console.log("-------- error ------- " + error))

    }
    render() {
        return (
            <View style={{ backgroundColor: 'white', width: '100%', height: '100%' }}>

                <View style={{ padding: 8 }}>
                    <View style={{ margin: 5 }}>
                        {/* {this.state.data.length > 0 && <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>{Constant.AppText.yourOrder}</Text>
                        } */}
                        <View style={{ marginTop: 10, paddingBottom: 20 }}>
                            <FlatList
                                data={this.state.data}
                                numColumns={1}
                                renderItem={({ item }) =>
                                    <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', padding: 15 }}>
                                        <View>
                                            <Image
                                                style={{ width: 55, height: 55 }}
                                                source={{ uri: `${this.state.imagesUrl}${item.pimg}` }} />
                                        </View>
                                        <View style={{ flex: 1, marginLeft: 10 }}>
                                            <Text style={{ textAlignVertical: 'center', fontWeight: 'bold', fontSize: 15, color: 'black' }}>{item.product_name.trim()}</Text>
                                            <Text style={{ textAlignVertical: 'center', fontSize: 14, color: 'black' }}>QTY : {item.qty}</Text>
                                            <Text style={{ textAlignVertical: 'center', fontSize: 14, color: 'black' }}>Price : {item.price}</Text>
                                        </View>
                                        <View>
                                            <Text style={{ textAlign: 'right', fontWeight: 'bold', fontSize: 16, color: 'black' }}>Total Price</Text>
                                            <Text style={{ textAlign: 'right', fontWeight: 'bold', fontSize: 16, color: Constant.AppColor.appTheme, margin: 2 }}>{item.total_price}</Text>
                                        </View>
                                    </View>
                                }
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    </View>
                </View>
                {
                    this.state.isLoading &&
                    <Loader loading={true} color="#0D788B" size="large"></Loader>
                }
                {
                    this.state.noDataFound &&
                    <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            style={{ height: 100, width: 100 }}
                            source={images.error} />
                        <Text style={{ padding: 10, fontSize: 22, fontWeight: 'bold' }}>No record found</Text>
                    </View>
                }
            </View >
        )
    }
}