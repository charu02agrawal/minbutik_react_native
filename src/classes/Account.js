import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Alert, TextInput } from "react-native";
import { _storeData } from '../sharedPrefernce/SharedPreference'
import Constant from '../../Constant'
import { _retrieveData } from '../sharedPrefernce/SharedPreference'
import Loader from './Loader';

export default class Account extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: '',
            isLoading: false,
            data: {}
        };

    }
    componentDidMount() {
        this.setState({ isLoading: true })
        _retrieveData('UserId').then(status => {
            console.log('user Id ***** ', status)
            this.setState({ user_id: status })
            this.getProfile()
        }).catch(err => {
            this.setState({ isLoading: false })

            console.log('err')
        })
    }

    getProfile() {
        let url = `${Constant.API.BASE_URL}${Constant.API.GET_PROFILE}`

        console.log('url', url)
        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                // latitudes: this.state.currentLatitude,
                // longitude: this.state.currentLongitude,
                user_id: this.state.user_id,
            })
        }).then(function (response) {
            console.log('response', response)
            return response.json();
        })
            .then(function (result) {
                return result;

            }).then(result => {
                console.log('PROFILE result', result)
                if (result.status === 'true') {
                    this.setState({
                        data: result.data,
                    })
                }

            })
            .then(isLoading => {
                this.setState({ isLoading: false });
            })
            .catch(error => console.log("-------- error ------- " + error))

    }

    onLogOut() {
        Alert.alert(
            '',
            'Are you sure you want to logout?',
            [
                {
                    text: 'Cancel',
                    style: 'cancel',

                },
                {
                    text: 'Ok',
                    onPress: () => {
                        _storeData('LoginStatus', 'LoggedOut')
                        this.props.navigation.navigate('Auth')
                    }
                },
            ],
            { cancelable: false },
        );
    }
    render() {
        return (
            <View style={{ padding: 10, marginTop: 40 }}>
              <View>
                <View >
                        <View style={{ marginVertical: 10 }}>
                            <Text style={{ paddingVertical: 5, color: 'red' }}>{this.state.error}</Text>
                            <Text>{Constant.AppText.name}</Text>
                        <TextInput
                            editable={false}
                                value={this.state.data.name}
                                returnKeyType='next'
                                style={{ borderBottomColor: 'gray', borderBottomWidth: 2 }}
                            />
                        </View>
                        <View style={{ marginVertical: 10 }}>
                            <Text>{Constant.AppText.email}</Text>
                        <TextInput
                            editable={false}
                                value={this.state.data.email}
                                autoCapitalize='none'
                                returnKeyType='next'
                                keyboardType='email-address'
                                style={{ borderBottomColor: 'gray', borderBottomWidth: 2 }}
                            />
                        </View>
                        <View style={{ marginVertical: 10 }}>
                            <Text>{Constant.AppText.mobile}</Text>
                            <TextInput
                                value={this.state.data.mobile}
                                editable ={false}
                                returnKeyType='next'
                                keyboardType='phone-pad'
                                style={{ borderBottomColor: 'gray', borderBottomWidth: 2 }}
                            />
                        </View>
                    </View>
                
                <View style={{ marginTop: 40 }}>
                    <TouchableOpacity
                        onPress={() => { this.props.navigation.replace('UpdateProfile') }}
                        style={{ backgroundColor: 'white', borderWidth: 1, borderColor: 'black', alignItems: 'center', padding: 10, margin: 5 }}>
                        <Text style={{ color: 'black', fontWeight: 'bold' }}>Update Profile</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.onLogOut()}
                        // onPress={() => { this.props.navigation.navigate('App') }}
                        style={{ width: '100%', marginTop: 20, backgroundColor: '#cc1dba', borderRadius: 5, alignItems: 'center', padding: 10 }}>
                        <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 18 }}>{Constant.AppText.logout}</Text>
                    </TouchableOpacity>
                </View>
                </View>
                    {this.state.isLoading &&
                    <Loader loading={true} color="#0D788B" size="large"></Loader>
                }
            </View>
        )
    }
}