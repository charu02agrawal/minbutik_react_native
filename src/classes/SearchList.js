import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, Platform, PermissionsAndroid } from "react-native";
import { images } from '../../Assets';
import Geolocation from '@react-native-community/geolocation';
import Loader from './Loader'
import Constant from '../../Constant';
import Geocoder from 'react-native-geocoding';
import { TextInput } from 'react-native-gesture-handler';
export default class SearchList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            imagesUrl: '',
            isLoading: false,
            currentLongitude: 'unknown',//Initial Longitude
            currentLatitude: 'unknown',//Initial Latitude
            noDataFound:false
        };
    }
    componentDidMount() {

        this._navListener = this.props.navigation.addListener('didFocus', () => {
            // get your new data here and then set state it will rerender
            console.log("did focus history");

            this.setState({
                data: [],
                imagesUrl: '',
                isLoading: false,
                currentLongitude: 'unknown',//Initial Longitude
                currentLatitude: 'unknown',//Initial Latitude
                noDataFound: false
            })
            console.log('componentDidMount')
            this.setState({ isLoading: true })
            var that = this;
            //Checking for the permission just after component loaded
            if (Platform.OS === 'ios') {
                this.callLocation(that);
            } else {
                async function requestLocationPermission() {
                    try {
                        const granted = await PermissionsAndroid.request(
                            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
                            'title': 'Location Access Required',
                            'message': 'This App needs to Access your location'
                        }
                        )
                        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                            //To Check, If Permission is granted
                            that.callLocation(that);
                        } else {
                            alert("Permission Denied");
                        }
                    } catch (err) {
                        alert("err", err);
                        console.warn(err)
                    }
                }
                requestLocationPermission();
            }
        });
    }
    callLocation(that) {
        console.log('location called')

        // Geocoder.from(22.7196, 75.8577)
        
        //     .then(json => {
                
        //         var addressComponent = json.results[0].address_components[0];
                
        //         console.log('-----add',addressComponent);
                
        //     })
        
        //     .catch(error =>
                
        // console.log('----err',error)

        //     );
        //alert("callLocation Called");
        Geolocation.getCurrentPosition(
            //Will give you the current location
            (position) => {
                console.log('position----',position)
                const currentLongitude = JSON.stringify(position.coords.longitude);
                //getting the Longitude from the location json
                const currentLatitude = JSON.stringify(position.coords.latitude);
                //getting the Latitude from the location json
                console.log('currentLongitude', currentLongitude)
                that.setState({ currentLongitude: currentLongitude });
                //Setting state Longitude to re re-render the Longitude Text
                that.setState({ currentLatitude: currentLatitude });
                //Setting state Latitude to re re-render the Longitude Text
                this.getDataFromServer()

                
            },
            (error) => {
                this.setState({ isLoading: false });

                alert(error.message)
            }
            ,
            { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
        );
        that.watchID = Geolocation.watchPosition((position) => {
            //Will give you the location on location change
            console.log('watchPosition----', position)
            const currentLongitude = JSON.stringify(position.coords.longitude);
            //getting the Longitude from the location json
            const currentLatitude = JSON.stringify(position.coords.latitude);
            //getting the Latitude from the location json
            that.setState({ currentLongitude: currentLongitude });
            //Setting state Longitude to re re-render the Longitude Text
            that.setState({ currentLatitude: currentLatitude });
            //Setting state Latitude to re re-render the Longitude Text
            console.log('watchPosition');
            this.getDataFromServer()


        });
        console.log('lat', this.state.currentLatitude)
        console.log('lng', this.state.currentLongitude)
        // if (this.state.currentLatitude != 'unknown' && this.state.currentLongitude != 'unknown') {
        //     console.log('api call')
        //     this.getDataFromServer()
        // }
        // this.getDataFromServer()

    }
    componentWillUnmount = () => {
        this._navListener.remove();

        Geolocation.clearWatch(this.watchID);
    }

    getDataFromServer() {
        let url = `${Constant.API.BASE_URL}${Constant.API.STORE_NEAR_BY}`
        console.log('lat url', url)

        console.log('lat',this.state.currentLatitude)
        console.log('lng',this.state.currentLongitude)

        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                latitudes: this.state.currentLatitude,
                longitude: this.state.currentLongitude,
                // latitudes: '22.7196',
                // longitude: '75.8577',
                mile: 300,
            })
        }).then(function (response) {
            console.log('response', response)
            return response.json();
        })
            .then(function (result) {
                return result;

            }).then(result => {
                console.log('result', result)
                if (result.status === 'true') {
                    this.setState({
                        data: result.data,
                        imagesUrl: result.imagesUrl
                    })
                } else {
                    this.setState({
                        noDataFound: true,
                    }) 
                }

            })
            .then(isLoading => {
                this.setState({ isLoading: false });
            })
            .catch(error => {
                this.setState({ isLoading: false });
                console.log("-------- error ------- " + error)
            })

    }
    render() {
        return (
            <View style={{ backgroundColor: 'white', width: '100%', height: '100%' }}>

                <View style={{ padding: 8 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: 10 }}>
                        <View style={{ flexDirection: 'row', flex: 1 }}>
                            <Image
                                style={{ width: 22, height: 22, margin: 5, tintColor: 'black' }}
                                source={images.menu} />
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                                onPress={() => { this.props.navigation.navigate('Cart') }}>
                                <Image
                                    style={{ width: 22, height: 22, margin: 5, tintColor: 'black' }}
                                    source={images.cart} />
                            </TouchableOpacity>
                            <Image
                                style={{ width: 22, height: 22, margin: 5, marginLeft: 20, tintColor: 'black' }}
                                source={images.user} />
                        </View>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        marginVertical: 10, elevation: 5,
                        backgroundColor: '#DCDCDC',
                        shadowOffset: { width: 0, height: 1 },
                        shadowColor: '#333',
                        shadowOpacity: 0.3,
                        shadowRadius: 2,
                        justifyContent: 'space-around',
                        alignItems: 'center'
                    }}>
                        <Image style={{ width: 20, height: 20, marginHorizontal: 15, resizeMode: 'cover', tintColor: 'gray' }}
                            source={images.search}
                        />
                        <View style={{ flex: 1 }}>
                            <TextInput
                                placeholder="Search for iems and Store"
                            />
                        </View>
                    </View>

                    <View style={{ margin: 5 }}>
                        {this.state.data.length > 0 && <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>{Constant.AppText.storeNearYou}</Text>
                        }
                            <View style={{ marginTop: 10, paddingBottom: 20 }}>
                            <FlatList
                                data={this.state.data}
                                numColumns={1}
                                renderItem={({ item }) =>
                                    <TouchableOpacity
                                        onPress ={()=>{this.props.navigation.navigate('ProductScreen',{store_id : item.id})}}
                                        style={{ height: 100, width: '100%', marginBottom: 10, flexDirection: 'row', padding: 2 }}>
                                        <View style={{
                                            height: 90, width: 120, elevation: 5,
                                            backgroundColor: '#DCDCDC',
                                            shadowOffset: { width: 0, height: 1 },
                                            shadowColor: '#333',
                                            shadowOpacity: 0.3,
                                            shadowRadius: 2,
                                            justifyContent: 'space-around',
                                            alignItems: 'center'
                                        }}>
                                            {/* {console.log('image url', `http://meebasket.com/eko2/${item.logo}`)} */}
                                            <Image style={{ width: '100%', height: 80, resizeMode: 'cover' }}
                                                source={{ uri: `${this.state.imagesUrl}${item.logo}` }}
                                            />
                                        </View>
                                        <View style={{ flex: 1, marginLeft: 20, marginTop: 4 }}>
                                            <Text style={{ color: 'gray' }}>Vegetables</Text>
                                            <Text style={{ color: 'black', fontWeight: 'bold' }}>{item.store_name}</Text>
                                            <View style={{ flexDirection: 'row', paddingTop: 5 }}>
                                                <Text style={{ color: '#cc1dba', fontWeight: 'bold' }}>{parseFloat(item.distance).toFixed(1)} km</Text>
                                                <Text style={{ paddingLeft: 15, color: 'gray' }}> mins</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    </View>
                </View>
                {
                    this.state.isLoading &&
                    <Loader loading={true} color="#0D788B" size="large"></Loader>
                }
                {
                    this.state.noDataFound && 
                    <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            style={{ height: 100, width: 100 }}
                            source={images.error} />
                        <Text style ={{padding:10,fontSize:22,fontWeight:'bold'}}>No record found</Text>
                    </View>
                }
            </View >
        )
    }
}