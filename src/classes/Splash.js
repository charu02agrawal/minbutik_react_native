import React, { Component } from 'react'
import {_retrieveData} from '../sharedPrefernce/SharedPreference'
import { View,Text } from 'react-native';
import Constant from '../../Constant';
export default class Splash extends React.Component{
    async componentDidMount() {
        _retrieveData('LoginStatus').then(status => {
            console.log('login ', status)
            if (status === 'Loggedin') {
                this.props.navigation.navigate('App')
            } else {
                this.props.navigation.replace('MainScreen')
            }

        }).catch(err => console.log('err'));

    }

    render() {
        return (
            <View style ={{flex:1,alignItems:'center',justifyContent:'center',}}>
                <View style={{  alignItems: 'center' }}>
                    <View style={{ height: 80, width: 80, backgroundColor: Constant.AppColor.appTheme, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 44 }}>MB</Text>
                    </View>
                    <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 22 }}>{Constant.AppText.appName}</Text>
                </View>
            </View>
        )
    }
}