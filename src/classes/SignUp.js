import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from "react-native";
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import Loader from './Loader';
import Constant from '../../Constant';
export default class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '', email: '', password: '', phone: '',
            error:'',
            isLoading: false,

        };

    }

    isEmptyInputField() {
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var isEmpty = true
        this.setState({ error: '' })

        if (this.state.name.trim().length === 0) {
            this.setState({error:'Name is required'})
            isEmpty = true
        } else if (emailRegex.test(this.state.email.trim()) === false) {
            isEmpty = true
            this.setState({ error: 'Please enter valid email address' })
        }
        else if (this.state.phone.trim().length < 10) {
            this.setState({ error: 'Please enter valid phone number' })
            isEmpty = true
        } else if (this.state.password.trim().length < 4) {
            this.setState({ error: 'Password must be 4 character' })
            isEmpty = true
        }
        else {
            isEmpty = false
        }
        console.log("Empty field value ", isEmpty)

        return isEmpty
    }

    

    postDataOnServer = () => {
        const {navigate} = this.props.navigation
        console.log('is empty', this.isEmptyInputField())
        if (this.isEmptyInputField() === false) {
            this.setState({ isLoading: true })
            // let data = this.createFormData();

            let url = `${Constant.API.BASE_URL}${Constant.API.SIGN_UP}`
            console.log("RUk API URL", url)
            fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    phone: this.state.phone,
                    name: this.state.name,
                    password: this.state.password,
                    email: this.state.email
                })
            }).then(function (response) {
                console.log('response', response)
                return response.json();
            })
                .then(function (result) {
                    return result;

                }).then(result => {
                // navigate('Login')
                    if (result.status === 'true') {
                     this.props.navigation.replace('Login')
                    } else {
                        this.setState({error:result.message})
                    }

            })
                .then(isLoading => {
                    this.setState({ isLoading: false });
                })
                .catch(error => console.log("-------- error ------- " + error))
        }
    }

    render() {
        return (
            <View style={{ backgroundColor: 'white', width: '100%', height: '100%' }}>

                <ScrollView>
                    <View>
                        <View style={{ margin: 15 }}>
                            {/* <View style={{ marginBottom: 10, marginTop: 30 }}>
                        <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 26 }}>Sign UP</Text>
                    </View> */}
                            <View style={{ marginTop: 50, marginBottom: 10 }}>
                                <Text style={{ paddingVertical: 5,color:'red' }}>{this.state.error}</Text>
                                <Text>{Constant.AppText.name}</Text>
                                <TextInput
                                    onChangeText={(name) =>
                                        this.setState({ name })}
                                    returnKeyType='next'
                                    style={{ borderBottomColor: 'gray', borderBottomWidth: 2 }}
                                />
                            </View>
                            <View style={{ marginVertical: 10 }}>
                                <Text>{Constant.AppText.email}</Text>
                                <TextInput
                                    onChangeText={(email) =>
                                        this.setState({ email })}
                                    autoCapitalize= 'none'
                                    returnKeyType='next'
                                    keyboardType='email-address'
                                    style={{ borderBottomColor: 'gray', borderBottomWidth: 2 }}
                                />
                            </View>
                            <View style={{ marginVertical: 10 }}>
                                <Text>{Constant.AppText.mobile}</Text>
                                <TextInput
                                    onChangeText={(phone) =>
                                        this.setState({ phone })}
                                    maxLength={10}
                                    returnKeyType='next'
                                    keyboardType='phone-pad'
                                    style={{ borderBottomColor: 'gray', borderBottomWidth: 2 }}
                                />
                            </View>
                            <View style={{ marginVertical: 10 }}>
                                <Text>{Constant.AppText.password}</Text>
                                <TextInput
                                    onChangeText={(password) =>
                                        this.setState({ password })}
                                    secureTextEntry={true}
                                    style={{ borderBottomColor: 'gray', borderBottomWidth: 2 }}
                                />
                            </View>
                            <TouchableOpacity
                                onPress={() => { this.postDataOnServer() }}

                                // onPress={() => { this.props.navigation.navigate('Verification') }}
                                style={{ width: '100%', marginTop: 20, backgroundColor: '#cc1dba', borderRadius: 5, alignItems: 'center', padding: 10 }}>
                                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 18 }}>{Constant.AppText.signUp}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                {this.state.isLoading &&
                    <Loader loading={true} color="#0D788B" size="large"></Loader>
                }
            </View>

        )
    }
}