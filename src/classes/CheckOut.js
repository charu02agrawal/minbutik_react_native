import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from "react-native";
import RadioButton from './RadioButton';

export default class CheckOut extends React.Component {
    constructor() {
        super();

        this.state = {
            radioItems:
                [
                    {
                        label: 'COD',
                        size: 22,
                        color: '#cc1dba',
                        selected: false
                    },

                    {
                        label: 'Net Banking',
                        color: '#cc1dba',
                        size: 22,
                        selected: true,
                    }
                ], selectedItem: ''
        }
    }

    componentDidMount() {
        this.state.radioItems.map((item) => {
            if (item.selected == true) {
                this.setState({ selectedItem: item.label });
            }
        });
    }

    changeActiveRadioButton(index) {
        this.state.radioItems.map((item) => {
            item.selected = false;
        });

        this.state.radioItems[index].selected = true;

        this.setState({ radioItems: this.state.radioItems }, () => {
            this.setState({ selectedItem: this.state.radioItems[index].label });
        });
    }
    render() {
        return (
            <View>
                <View style={{ padding: 15, marginTop: 40 }}>
                    <Text style={{ fontSize: 16 }}>Delivery Date</Text>
                    <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 20, marginTop: 3 }}>Thrusday , 12 June</Text>
                    <Text style={{ fontSize: 18 }}>1 PM</Text>
                    <View style={{ marginTop: 25 }}>
                        <Text style={{ fontSize: 16 }}>Delivery address</Text>
                        <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 20, marginTop: 3 }}>Home</Text>
                        <Text style={{ fontSize: 18 }}>33 New road Sweden , 444444</Text>
                        <Text style={{ fontSize: 16, marginTop: 30 }}>Payment</Text>
                        <View style={{ flexDirection: 'row' }}>

                        {
                            this.state.radioItems.map((item, key) =>
                                (
                                    <View style ={{flex:1,alignItems:'flex-start'}}>
                                    <RadioButton key={key} button={item} onClick={this.changeActiveRadioButton.bind(this, key)} />
                                </View>
                                ))
                            }
</View>
                        <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 18, marginTop: 20 }}>Summary</Text>

                    </View>
                    <View style={{ width: '100%', marginTop: 20 }}>
                        <View style={{ width: '100%', flexDirection: 'row' }}>
                            <Text style={{ flex: 1 }}>Milk * 1</Text>
                            <Text style={{ flex: 1, textAlign: 'right' }}>SEK 10</Text>
                        </View>
                        <View style={{ width: '100%', flexDirection: 'row', marginTop: 5 }}>
                            <Text style={{ flex: 1 }}>Fresh vegetables * 1</Text>
                            <Text style={{ flex: 1, textAlign: 'right' }}>SEK 10</Text>
                        </View>
                        <View style={{ width: '100%', flexDirection: 'row', marginTop: 5 }}>
                            <Text style={{ flex: 1 }}>Fruits * 2</Text>
                            <Text style={{ flex: 1, textAlign: 'right' }}>SEK 30</Text>
                        </View>
                        <View style={{ width: '100%', flexDirection: 'row', marginTop: 15 }}>
                            <Text style={{ flex: 1, fontWeight: 'bold', color: 'black', fontSize: 18 }}>Total Price</Text>
                            <Text style={{ flex: 1, textAlign: 'right', fontWeight: 'bold', color: 'black', fontSize: 18 }}>SEK 30</Text>
                        </View>
                        <TouchableOpacity
                            onPress={() => { this.props.navigation.navigate('Success') }}
                            style={{ marginTop: 55, backgroundColor: '#cc1dba', borderWidth: 1, borderColor: 'black', borderRadius: 5, alignItems: 'center', padding: 10 }}>
                            <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 16 }}>Confirm Order</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}