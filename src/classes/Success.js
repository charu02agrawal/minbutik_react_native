import React, { Component } from 'react';
import { View, Text, TouchableOpacity,Image } from "react-native";
import { images } from '../../Assets';

export default class Success extends React.Component {
    render() {
        return (
            <View>
                <View style={{ alignItems: 'center', marginTop: 120, padding: 15 }}>
                    <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 32 }}>Success!</Text>
                    <View style ={{alignItems:'center',padding:10,marginTop:20}}>
                        <Image
                            style={{ height: 60, width: 60, tintColor:'#cc1dba' }}
                            source ={images.success}
                        />
                    </View>
                    <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 18, marginTop: 10 }}>Thankyou for shopping with us</Text>
                    <View style={{ padding: 10 }}>
                        <Text style={{ fontSize: 24, marginTop: 10, textAlign: 'center' }}>Your items has been placed and is on it's way to process.</Text>
                    </View>
                    <TouchableOpacity
                        style={{ width :'100%',margin: 15, marginTop:50,backgroundColor: '#cc1dba', borderRadius: 5, alignItems: 'center', padding: 10 }}>
                        <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 16 }}>Track Order</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ width:'100%',margin: 15, backgroundColor: 'white', borderWidth: 1, borderColor: '#cc1dba', borderRadius: 5, alignItems: 'center', padding: 10 }}>
                        <Text style={{ fontWeight: 'bold', color: '#cc1dba', fontSize: 16 }}>Continue Shopping</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}