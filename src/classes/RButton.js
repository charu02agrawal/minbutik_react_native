import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { images } from '../../Assets'

export default class RButton extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onClick} activeOpacity={0.8} style={styles.radioButton}>
                {/* <View style={[styles.radioButtonHolder, { height: this.props.button.size, width: this.props.button.size, borderColor: 'gray' }]}> */}
                    <Text style={[styles.label, { color: 'black' }]}>{this.props.button.label}</Text>

                    {
                        (this.props.button.selected)
                            ?
                        (
                                <Image
                                    style={{ height: this.props.button.size, width: this.props.button.size }}
                                    source={images.select} />
                                // <View style={[styles.radioIcon, { height: this.props.button.size, width: this.props.button.size, backgroundColor: this.props.button.color }]}></View>
                            )
                            :
                                <View style={[styles.radioIcon, { height: this.props.button.size, width: this.props.button.size, borderColor: 'gray',borderWidth:2 }]}></View>
                    }
                {/* </View> */}
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    radioButton: {
        flexDirection: 'row',
        padding: 10,
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    radioButtonHolder: {
        borderRadius: 50,
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    radioIcon: {
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        flex:1,
        marginLeft: 10,
        marginRight:30,
        fontSize: 16,
        fontWeight:'bold'
    },

});