import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, Platform, PermissionsAndroid } from "react-native";
import { images } from '../../Assets';
import Geolocation from '@react-native-community/geolocation';
import Loader from './Loader'
import Constant from '../../Constant';
export default class Category extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            imagesUrl: '',
            isLoading: false,
            qty: 0,
            noDataFound: false,
            cate_id: this.props.navigation.state.params.cate_id
        };
    }
    componentDidMount() {

        this.getDataFromServer()
    }

    getDataFromServer() {
        let url = `${Constant.API.BASE_URL}${Constant.API.CATEGORY_BY_PRODUCT}`
        console.log('url', url)
        this.setState({ isLoading: true });

        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                cate_id: this.state.cate_id,
            })
        }).then(function (response) {
            console.log('response', response)
            return response.json();
        })
            .then(function (result) {
                return result;

            }).then(result => {
                console.log('result', result)
                if (result.status === 'true') {
                    this.setState({
                        data: result.data,
                        imagesUrl: result.imagesUrl
                    })
                } else {
                    this.setState({
                        noDataFound: true,
                    })
                }

            })
            .then(isLoading => {
                this.setState({ isLoading: false });
            })
            .catch(error => console.log("-------- error ------- " + error))

    }
    render() {
        return (
            <View style={{ backgroundColor: 'white', width: '100%', height: '100%' }}>

                <View style={{ padding: 8 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: 10 }}>
                        <View style={{ flexDirection: 'row', flex: 1 }}>
                            <Image
                                style={{ width: 22, height: 22, margin: 5, tintColor: 'black' }}
                                source={images.menu} />
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                                onPress={() => { this.props.navigation.navigate('Cart') }}>
                                <Image
                                    style={{ width: 22, height: 22, margin: 5, tintColor: 'black' }}
                                    source={images.cart} />
                            </TouchableOpacity>
                            <Image
                                style={{ width: 22, height: 22, margin: 5, marginLeft: 20, tintColor: 'black' }}
                                source={images.user} />
                        </View>
                    </View>
                    <View style={{ margin: 5 }}>

                        <View style={{ marginTop: 10, paddingBottom: 20 }}>
                            <FlatList
                                data={this.state.data}
                                numColumns={1}
                                renderItem={({ item, index }) =>
                                    <TouchableOpacity
                                        onPress={() => { this.props.navigation.navigate('ProductDetail', { item: item, imagesUrl: this.state.imagesUrl }) }}
                                        style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', padding: 15 }}>
                                        <View style={{
                                            height: 55, elevation: 5,
                                            backgroundColor: '#DCDCDC',
                                            shadowOffset: { width: 0, height: 1 },
                                            shadowColor: '#333',
                                            shadowOpacity: 0.3,
                                            shadowRadius: 2,
                                            justifyContent: 'space-around',
                                            alignItems: 'center'
                                        }}>
                                            <Image
                                                style={{ width: 55, height: 55 }}
                                                source={{ uri: `${this.state.imagesUrl}${item.pimg}` }} />
                                        </View>
                                        <View style={{ flex: 1, marginLeft: 10 }}>
                                            <Text style={{ textAlignVertical: 'center', fontWeight: 'bold', fontSize: 15, color: 'black' }}>{item.pname.trim()}</Text>
                                            <Text style={{ textAlignVertical: 'center', fontSize: 14, color: 'black' }}>QTY : {item.stock}</Text>

                                        </View>
                                        <View>
                                            <Text style={{ textAlign: 'right', fontWeight: 'bold', fontSize: 16, color: 'black' }}>Price : {item.pprice}</Text>
                                            {/* <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        if (this.state.qty > 0) {
                                                            console.log('large')
                                                            this.setState({ qty: this.state.qty - 1 })
                                                        }
                                                    }}
                                                    style={{ alignItems: 'center', justifyContent: 'center', margin: 2 }}>
                                                    <Image
                                                        style={{ width: 20, height: 20 }}
                                                        source={images.minus} />
                                                </TouchableOpacity>
                                                <Text style={{ textAlignVertical: 'center', fontWeight: 'bold', fontSize: 16, color: 'black', margin: 2 }}>{this.state.qty}</Text>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.setState({ qty: this.state.qty + 1 })

                                                    }}
                                                    style={{ alignItems: 'center', justifyContent: 'center', margin: 2 }}>
                                                    <Image
                                                        style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                                        source={images.plus} />
                                                </TouchableOpacity>

                                            </View>
                                        */}
                                        </View>

                                    </TouchableOpacity>
                                }
                                keyExtractor={(item, index) => index.toString()}

                            />
                            {/* <FlatList
                                data={this.state.data}
                                numColumns={1}
                                renderItem={({ item }) =>
                                    <TouchableOpacity
                                        onPress={() => { alert('click') }}
                                        style={{ height: 100, width: '100%', marginBottom: 10, flexDirection: 'row', padding: 2 }}>
                                        <View style={{
                                            height: 90, width: 120, elevation: 5,
                                            backgroundColor: '#DCDCDC',
                                            shadowOffset: { width: 0, height: 1 },
                                            shadowColor: '#333',
                                            shadowOpacity: 0.3,
                                            shadowRadius: 2,
                                            justifyContent: 'space-around',
                                            alignItems: 'center'
                                        }}>
                                            <Image style={{ width: '100%', height: 80, resizeMode: 'cover' }}
                                                source={{ uri: `http://meebasket.com/eko2/${item.logo}` }}
                                            />
                                        </View>
                                        <View style={{ flex: 1, marginLeft: 20, marginTop: 4 }}>
                                            <Text style={{ color: 'gray' }}>Vegetables</Text>
                                            <Text style={{ color: 'black', fontWeight: 'bold' }}>{item.store_name}</Text>
                                            <View style={{ flexDirection: 'row', paddingTop: 5 }}>
                                                <Text style={{ color: '#cc1dba', fontWeight: 'bold' }}>{parseFloat(item.distance).toFixed(1)} km</Text>
                                                <Text style={{ paddingLeft: 15, color: 'gray' }}> mins</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                                keyExtractor={(item, index) => index.toString()}
                            /> */}
                        </View>
                    </View>
                </View>
                {
                    this.state.isLoading &&
                    <Loader loading={true} color="#0D788B" size="large"></Loader>
                }
                {
                    this.state.noDataFound &&
                    <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            style={{ height: 100, width: 100 }}
                            source={images.error} />
                        <Text style={{ padding: 10, fontSize: 22, fontWeight: 'bold' }}>No record found</Text>
                    </View>
                }
            </View >
            
        )
    }
}