import { AsyncStorage } from 'react-native';


export async function _storeData(key, value) {
    // console.log("Storage key: " + key + "Value: " + value)
    try {
        // console.log(key, value)
        await AsyncStorage.setItem(key, value);
    } catch (error) {
        console.error('', error);
        return false;
    }

    return true;
};

export const _retrieveData = (key) => {
    // console.log('_retrieveData', key)
    return AsyncStorage.getItem(key);//.then(res=>callback(res)).catch(err=>err);

};

export const _header = () => {
    let header = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Content-Type': 'multipart/form-data',
        Authorization: _retrieveData('accessToken'),
        sessionToken: _retrieveData('sessionToken'),
    }
    // console.log("HEADER", header);
    return header
}