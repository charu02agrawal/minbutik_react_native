import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import {images} from '../Assets'
import React, { Component } from 'react';
import MainScreen from '../src/classes/MainScreen'
import SignUp from '../src/classes/SignUp';
import Login from '../src/classes/Login';
import Search from '../src/classes/SearchList';
import Home from '../src/classes/Home';
import Order from '../src/classes/Order';
import WishList from '../src/classes/WishList';
import Account from '../src/classes/Account';
import Verification from '../src/classes/Verification';
import ProductScreen from '../src/classes/ProductScreen';

import { Image, Text } from 'react-native';
import Cart from '../src/classes/CartScreen';
import Success from '../src/classes/Success';
import Track from '../src/classes/Track';
import CheckOut from '../src/classes/CheckOut';
import Splash from '../src/classes/Splash';
import Category from '../src/classes/Category';
import ProductDetail from '../src/classes/ProductDetail';
import UpdateProfile from '../src/classes/UpdateProfile';

const AuthNavigator = createStackNavigator({

    Splash: {
        screen: Splash,
        navigationOptions: () => ({
            headerShown: false,
        }),
    },
    MainScreen: {
        screen: MainScreen,
        navigationOptions: () => ({
            headerShown: false,
        }),
    },
    Login: {
        screen: Login,
        navigationOptions: () => ({
            headerShown: true,
        }),
    },
    Signup: {
        screen: SignUp,
        navigationOptions: () => ({
            headerShown: true,
        }),
    },

    Track: {
        screen: Track,
        navigationOptions: ({ navigation }) => ({
            headerShown: false,
        }
            // this.commonNavigationOptionsWithRightImage('Now Tracking', navigation)
        ),
    },
    Verification: {
        screen: Verification,
        navigationOptions: ({ navigation }) => ({
            headerShown: false,
        }
            // this.commonNavigationOptionsWithRightImage('Now Tracking', navigation)
        ),
    },
}, {
    initialRouteName: 'Splash',
    headerMode: 'screen',
}
)

const DashboardNavigator = (initialViewName) => createStackNavigator({

    
    Search: {
        screen: Search,
        navigationOptions: ({ navigation }) => ({
            headerShown: false,
            
}
            // this.commonNavigationOptionsWithRightImage('Now Tracking', navigation)
        ),
    },
    Home: {
        screen: Home,
        navigationOptions: ({ navigation }) => ({
            headerShown: false,
}
            // this.commonNavigationOptionsWithRightImage('Now Tracking', navigation)
        ),
    },
    Order: {
        screen: Order,
        navigationOptions: ({ navigation }) => ({
            headerShown: true,
}
            // this.commonNavigationOptionsWithRightImage('Now Tracking', navigation)
        ),
    },
    WishList: {
        screen: WishList,
        navigationOptions: ({ navigation }) => ({
            headerShown: true,
}
            // this.commonNavigationOptionsWithRightImage('Now Tracking', navigation)
        ),
    },
    Account: {
        screen: Account,
        navigationOptions: ({ navigation }) => ({
            headerShown: true,
}
            // this.commonNavigationOptionsWithRightImage('Now Tracking', navigation)
        ),
    },
    ProductScreen: {
        screen: ProductScreen,
        navigationOptions: ({ navigation }) => ({
            headerShown: false,
        }
            // this.commonNavigationOptionsWithRightImage('Now Tracking', navigation)
        ),
    },
    ProductDetail: {
        screen: ProductDetail,
        navigationOptions: ({ navigation }) => ({
            headerShown: false,
        }
            // this.commonNavigationOptionsWithRightImage('Now Tracking', navigation)
        ),
    },
    CategoryScreen: {
        screen: Category,
        navigationOptions: ({ navigation }) => ({
            headerShown: false,
        }
            // this.commonNavigationOptionsWithRightImage('Now Tracking', navigation)
        ),
    },
    Cart: {
        screen: Cart,
        navigationOptions: ({ navigation }) => ({
            headerShown: true,
        }
            // this.commonNavigationOptionsWithRightImage('Now Tracking', navigation)
        ),
    },
    Success: {
        screen: Success,
        navigationOptions: ({ navigation }) => ({
            headerShown: false,
        }
            // this.commonNavigationOptionsWithRightImage('Now Tracking', navigation)
        ),
    },
  
    CheckOut: {
        screen: CheckOut,
        navigationOptions: ({ navigation }) => ({
            headerShown: true,
        }
            // this.commonNavigationOptionsWithRightImage('Now Tracking', navigation)
        ),
    },
    UpdateProfile: {
        screen: UpdateProfile,
        navigationOptions: ({ navigation }) => ({
            headerShown: true,
        }
            // this.commonNavigationOptionsWithRightImage('Now Tracking', navigation)
        ),
    },
},
    {
        initialRouteName: initialViewName,
        // headerMode: this.screenSize(),
    },
);

const setHeaderRight = state => {
    //console.log("setHeaderRight", this.state.secureTextEntry);
    return (
        <TouchableOpacity
            onPress={() => {
                this.maskPassword();
            }}
        >
            <Text>Hiii</Text>
            
        </TouchableOpacity>
    );
};

const AppTabNavigator = createBottomTabNavigator({
    SEARCH: DashboardNavigator('Search'),
    HOME: DashboardNavigator('Home'),
    ORDER: DashboardNavigator('Order'),
    WISHLIST: DashboardNavigator('WishList'),
    ACCOUNT: DashboardNavigator('Account')
},
  
    {
        defaultNavigationOptions: ({ navigation }) => ({
        
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
            const { routeName } = navigation.state;;
            if (routeName === 'SEARCH') {
                return <Image
                    source={images.search}
                    style={{ height: 20, width: 20, tintColor: tintColor }}
                />;
            }
            else if (routeName === 'HOME') {
                return <Image
                    source={images.home}
                    style={{ height: 20, width: 20, tintColor: tintColor }}
                />;
            } else if (routeName === 'WISHLIST') {
                return <Image
                    source={images.wishlist}
                    style={{ height: 18, width: 20, tintColor: tintColor }}
                />;
            } else if (routeName === 'ORDER') {
                return <Image
                    source={images.order}
                    style={{ height: 18, width: 20, tintColor: tintColor }}
                />;
            }
            else {
                return <Image
                    source={images.account}
                    style={{ height: 20, width: 20, tintColor: tintColor }}
                />;
            }
            },
            
        }),
        initialRouteName:'HOME',
    tabBarOptions: {
        activeTintColor: '#cc1dba',
        // activeTintColor: Constant.AppColor.green, //For changing tint colors
        inactiveTintColor: 'gray',
    },
    },
);


const AppSwitch = createSwitchNavigator(
    {
        Auth: AuthNavigator,
        App: AppTabNavigator,
    },
    {
        initialRouteName: "Auth",
    }
);

const RootScreen = createAppContainer(AppSwitch);

export default RootScreen;