export default {
    
    API: {
        BASE_URL: 'https://meebasket.com/eko2/Api/',
        LOGIN: 'userLogin',
        SIGN_UP: 'userSingup',
        CATEGORY_ALL_DATA: 'categogyAllData',
        STORE_NEAR_BY: 'findStoreNearBy',
        STORE_BY_PRODUCT: 'storeByProduct',
        CATEGORY_BY_PRODUCT: 'categoryByProduct',
        SUB_CATEGORY_BY_PRODUCT: 'subcategoryByProduct',
        GET_ORDER : 'getOderByUserId',
        ORDER_PRODUCT : 'oderProduct',
        CART_DETAIL : 'CardDetailByUserId',
        ADD_TO_CART: 'addToCart',
        UPDATE_PROFILE: 'UpdateProfile',
        GET_PROFILE:'getProfile'
     },

    AppText: {
        email: 'Email',
        password: 'Password',
        login: 'Login',
        logout: 'Log Out',
        name: 'Name',
        mobile: 'Mobile Number',
        signUp:'Sign Up',
        appName:'MinButik',
        topCategory: 'Top Categories',
        storeNearYou: 'Stores near you',
        yourOrder: 'Your Orders',
        update:'Update',
    },

    fontType: {
        sfProTextRegular: '',
        dosisRegular: '',
        sourceSansProRegular: 'SourceSansPro-Regular',

        sfProTextSemiBold: '',
        dosisSemiBold: 'Dosis-SemiBold',
        sourceSansProSemiBold: 'SourceSansPro-SemiBold',

        sfProTextBold: '',
        dosisBold: 'Dosis-Bold',
        sourceSansProBold: 'SourceSansPro-Bold'
    },


    AppColor: {
        appTheme:'#cc1dba'
    }
};